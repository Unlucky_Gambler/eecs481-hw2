Used Visual Studio 2012 Ultimate, with Windows Phone SDK.

WPE Start.png shows the app when it is first run. After 
clicking the "Change Background Color" button, the background 
changes to a random color, as shown in WPE Color Change.png. 
The "Swap" button swaps the text in the 2 textfields, as 
shown in the other 2 .png files.